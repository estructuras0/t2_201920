package model.logic;

public class Viaje 

{
	//Atributos

	private String idOrigen;

	private String idDestino;

	private String hora;

	private String tiempoPromedio;

	private String desviacionEstandarViaje;

	private String tiempoPromedioSegundosGeometrico;

	private String desviacionEstandarGeometrica;

	//Constructor

	public Viaje(String pHora, String pIdOrigen, String pIdDestino, String pTiempopromedio, String pDesviacionEstandarViaje,
			String pTiempoPromedioGeometrico, String pDesviacionEstandarGeometrica)

	{

		tiempoPromedio=pTiempopromedio;
		idOrigen=pIdOrigen;
		idDestino=pIdDestino;
		hora=pHora;
		desviacionEstandarGeometrica=pDesviacionEstandarGeometrica;
		desviacionEstandarViaje=pDesviacionEstandarViaje;
		tiempoPromedioSegundosGeometrico=pTiempoPromedioGeometrico;

	}

	//Metodos

	public String darIdOrigen()
	{
		return idOrigen;

	}

	public String darIdDestino()
	{
		return idDestino;

	}

	public String darHora()
	{
		return hora;

	}

	public String darTiempoPromedio()
	{
		return tiempoPromedio;

	}

	public String darDesviacionEstandarViaje()
	{
		return desviacionEstandarViaje;

	}

	public String darDesviacionEstandarGeometrica()
	{

		return desviacionEstandarGeometrica;

	}

	public String darPromedioSegundosGeometrico()
	{
		return tiempoPromedioSegundosGeometrico;

	}

}

