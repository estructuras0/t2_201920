package main;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import controller.Controller;
import com.opencsv.CSVReader;

public class MVC {
	
	public static void main(String[] args) 
	{
		CSVReader reader = null;
		try {
			
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			for(String[] nextLine : reader) {
		       System.out.println("col1: " + nextLine[0] + ", col2: "+ nextLine[1]+ ","+ nextLine[2]+","+ nextLine[3]+","+ nextLine[4]+ ","+nextLine[5]+","+ nextLine[6] );
		    }
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally{
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		Controller controler = new Controller();
		controler.run();
	}
}
